#ifndef GAZEBO_PLUGINS_CheckpointModelPlugin_HH_
#define GAZEBO_PLUGINS_CheckpointModelPlugin_HH_

#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>

#include "ros/ros.h"
#include "riders_environments/checkpoint.h"

namespace gazebo
{

  class GAZEBO_VISIBLE CheckpointModelPlugin : public ModelPlugin
  {
    // Constructor
    public: CheckpointModelPlugin();
    // Deconstructor
    public: virtual ~CheckpointModelPlugin();
    // Load Function
    // Runs Once on Initialization
    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr /*_sdf*/);
    // Update Function
    // Updates on Every World Update
    private: virtual void OnUpdate();
    // Connection to World Update events.
    private: event::ConnectionPtr updateConnection;

    private: std::vector<std::string> GetAllCollisions(physics::ModelPtr target_model);

    private: std::vector<std::string> GetMessageCollisions(
        std::string model_name);

      private: std::vector<std::string> GetMessageCollisions(
          std::string model_name, std::string checkpoint_name);

    private: bool InsideCheckpoint(
        physics::ModelPtr target_model);

    private: std::string TouchingCheckpoint(
        physics::ModelPtr target_model);

    // Node Used for Communication.
    private: std::mutex mutex;
    // Pointer to the World
    private: physics::WorldPtr world;
    // Pointer to the Model
    private: physics::ModelPtr model;
    // Target Model Name
    // Argument in sdf
    private: std::string target_model_name;
    // Vector of Target Models
    private: std::vector<std::string> target_model_names;
    // Checkpoint type
    // Argument in SDF
    private: std::string checkpoint_type;
    // Target Model Pointer
    private: physics::ModelPtr target_model;
    // Vector of Target Link Collisions
    private: physics::Collision_V target_collisions;
    // World Simulation Time
    private: double sim_time;
    //Contact Sensor
    private: sensors::ContactSensorPtr contact_sensor;
    // Subscriber to Contact Sensor
    private: transport::SubscriberPtr Sub;
    //ROS Node Handler
    private: std::unique_ptr<ros::NodeHandle> rosNode;
    //ROS publisher
    private: ros::Publisher pub;

    private: riders_environments::checkpoint Checkpoint_msgs;

    private: std::vector<bool> debug_flag;
  };
}
#endif
