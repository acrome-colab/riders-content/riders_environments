#include "CheckpointModelPlugin.hh"

using namespace gazebo;
GZ_REGISTER_MODEL_PLUGIN(CheckpointModelPlugin)


// Constructor
CheckpointModelPlugin::CheckpointModelPlugin() : ModelPlugin()
{
}

// Deconstructor
CheckpointModelPlugin::~CheckpointModelPlugin()
{
}



// Runs once on initialization
void CheckpointModelPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
  // Model Pointer
  this->model = _model;
  // World Pointer Model is in
  this->world = this->model->GetWorld();
  // Get Checkpoint Type
  // touch by defaultUsage
  this->checkpoint_type = "touch";
  if (_sdf->HasElement("checkpoint_type")){
    this->checkpoint_type = _sdf->Get<std::string>("checkpoint_type");
  }
  //Get Contact Sensor
  if (_sdf->HasElement("contact_sensor")){
    std::string contact_sensor_name = _sdf->Get<std::string>("contact_sensor");
    // Get sensor Pointer by Name
    this->contact_sensor = std::dynamic_pointer_cast<sensors::ContactSensor>(
       sensors::SensorManager::Instance()->GetSensor(contact_sensor_name));
    //Check for Sensor
    if (!this->contact_sensor)
    {
      std::cout << "Couldn't find sensor [" << contact_sensor_name
                << "]" << '\n';
      return;
    }
    // Set Contact Sensor Active
    // This Must be Called
    this->contact_sensor->SetActive(true);

  }else{
    std::cout << "No inside sensor name entered" << '\n';
  }

  Checkpoint_msgs.type = checkpoint_type;
  Checkpoint_msgs.checkpoint_name = model->GetName();

  //Get Target Model Name
  if (_sdf->HasElement("target_model")){
    auto elem = _sdf->GetElement("target_model");

    while (elem)
    {
      target_model_name= elem->Get<std::string>();
      target_model_names.push_back(target_model_name);
      // Get Next Target
      elem = elem->GetNextElement("target_model");
    }
    debug_flag.resize(target_model_names.size());
  }else{
    std::cout << "Warning: No model name entered." << '\n';
  }

  // Might use it later
  //std::string desc = this->world->BaseByName(target_model_name)->GetSDF()->ToString("");

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, this->model->GetName(),
        ros::init_options::NoSigintHandler);
  }

  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle("checkpoint"));

  this->pub = this->rosNode->advertise<riders_environments::checkpoint>("checkpoint", 1000);

  // Connect Sensor Update Event
  this->updateConnection = this->contact_sensor->ConnectUpdated(
        std::bind(&CheckpointModelPlugin::OnUpdate, this));
}

// Update Callback
void CheckpointModelPlugin::OnUpdate()
{
  std::lock_guard<std::mutex> lock(this->mutex);
  msgs::Contacts contacts;
   contacts = this->contact_sensor->Contacts();

  for (std::vector<std::string>::iterator mi = target_model_names.begin();
    mi != target_model_names.end(); ++mi) {
    // Get Model Name
    std::string model_name = (*mi);
    target_model = this->world->ModelByName(model_name);
    // Check for Model
    if(target_model != NULL) {
      if(checkpoint_type == "touch") {

        std::string collision_name = CheckpointModelPlugin::TouchingCheckpoint(target_model);
        if(collision_name.size() != 0) {

          Checkpoint_msgs.collision = collision_name;
          Checkpoint_msgs.time = this->world->SimTime().Double();

          this->pub.publish(Checkpoint_msgs);
        }

       }else if (checkpoint_type == "inside") {       
        if(CheckpointModelPlugin::InsideCheckpoint(target_model)) {

          Checkpoint_msgs.collision = model_name;
          Checkpoint_msgs.time = this->world->SimTime().Double();

          this->pub.publish(Checkpoint_msgs);
        }
      }
    }
  }
}

// Gets all collision of specified model
std::vector<std::string> CheckpointModelPlugin::GetAllCollisions(physics::ModelPtr target_model) {

  std::string target_model_name = target_model->GetName();
  std::string target_link_name;
  std::string target_collision_name;
  std::string target_collision_full_name;
  std::vector<std::string> target_collisions;

  physics::Link_V target_links = target_model->GetLinks();

  for (physics::Link_V::iterator li = target_links.begin();
    li != target_links.end(); ++li) {

    target_link_name = (*li)->GetName();

    physics::Collision_V link_collision = (*li)->GetCollisions();
    for (physics::Collision_V::iterator ci = link_collision.begin();
      ci != link_collision.end(); ++ci) {

      target_collision_name = (*ci)->GetName();
      target_collision_full_name.clear();

      target_collision_full_name.append(target_model_name);
      target_collision_full_name.append("::");
      target_collision_full_name.append(target_link_name);
      target_collision_full_name.append("::");
      target_collision_full_name.append(target_collision_name);
      target_collisions.push_back(target_collision_full_name);

    }
  }
  return target_collisions;
}

std::vector<std::string> CheckpointModelPlugin::GetMessageCollisions(
    std::string model_name) {

  msgs::Contacts contacts = this->contact_sensor->Contacts();
  std::vector<std::string> message_collisions;
  std::string contact_name;

  for (int i = 0; i < contacts.contact_size(); ++i){

    std::string inside_contact_model = contacts.contact(
       i).collision1().substr(0, contacts.contact(
         i).collision1().find("::"));
    if(model_name == inside_contact_model) {

      contact_name.clear();
      contact_name = contacts.contact(i).collision1();
      message_collisions.push_back(contact_name);
    }
  }
  return message_collisions;
}

std::vector<std::string> CheckpointModelPlugin::GetMessageCollisions(
    std::string model_name, std::string checkpoint_name) {
  msgs::Contacts contacts = this->contact_sensor->Contacts();
  std::vector<std::string> message_collisions;
  std::string contact_name;
  for (int i = 0; i < contacts.contact_size(); ++i){

    std::string inside_contact_model = contacts.contact(
       i).collision1().substr(0, contacts.contact(
         i).collision1().find("::"));
    if(model_name == inside_contact_model) {
      if(contacts.contact(i).collision2() == this->model->GetName() + "::inside_checkpoint::" + checkpoint_name || 
          contacts.contact(i).collision2() == this->model->GetName() + "::border_checkpoint::" + checkpoint_name) {
          contact_name.clear();
          contact_name = contacts.contact(i).collision1();
          message_collisions.push_back(contact_name);
      }
    }
  }
  return message_collisions;
}

std::string CheckpointModelPlugin::TouchingCheckpoint(
    physics::ModelPtr target_model) {

  std::string touch_collision;
  std::vector<std::string> message_collisions;
  touch_collision.clear();

  if(this->checkpoint_type == "inside"){
      message_collisions =
          CheckpointModelPlugin::GetMessageCollisions(target_model->GetName(), "border_checkpoint");
  }else{
      message_collisions =
          CheckpointModelPlugin::GetMessageCollisions(target_model->GetName());
  }

  std::vector<std::string> target_collisions =
      CheckpointModelPlugin::GetAllCollisions(target_model);

  std::sort(message_collisions.begin(), message_collisions.end());
  std::sort(target_collisions.begin(), target_collisions.end());

  for (std::vector<std::string>::iterator msgi = message_collisions.begin();
    msgi != message_collisions.end(); ++msgi) {

    if (std::find(target_collisions.begin(), target_collisions.end(), (*msgi))
        != target_collisions.end()) {
      touch_collision = (*msgi);
      return touch_collision;
    }
  }
  return touch_collision;
}

bool CheckpointModelPlugin::InsideCheckpoint(
    physics::ModelPtr target_model) {

  std::vector<std::string> message_collisions =
      CheckpointModelPlugin::GetMessageCollisions(target_model->GetName(),  "inside_checkpoint");

  std::vector<std::string> target_collisions =
      CheckpointModelPlugin::GetAllCollisions(target_model);

  std::sort(message_collisions.begin(), message_collisions.end());
  std::sort(target_collisions.begin(), target_collisions.end());

  return (message_collisions == target_collisions) &&
      (CheckpointModelPlugin::TouchingCheckpoint(target_model).size()==0);
}
